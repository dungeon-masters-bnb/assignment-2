// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Ballot {
    uint256 public maxId;

    struct Proposal {
        address owner;
        string name;
        string[] voteOptions;
        uint256[] voteOptionsCount;
        address[] voters;
        bool closed;
        string winner;
    }

    mapping(uint256 => Proposal) public proposals;

    constructor() {
        maxId = 0;
    }

    function createProposal(
        string memory _name,
        string[] memory _voteOptions
    ) public {
        uint256[] memory _voteOptionsCount = new uint256[](_voteOptions.length);
        address[] memory _voters = new address[](0);
        Proposal memory newProposal = Proposal({
            owner: msg.sender,
            name: _name,
            voteOptions: _voteOptions,
            voteOptionsCount: _voteOptionsCount,
            voters: _voters,
            closed: false,
            winner: ""
        });
        proposals[maxId] = newProposal;
        maxId++;
    }

    function giveVoteFor(
        uint256 _proposalId,
        string memory _voteOption
    ) public {
        require(_proposalId <= maxId, "Wrong proposal ID!");
        Proposal storage targetProposal = proposals[_proposalId];
        require(!targetProposal.closed, "Voting is already closed!");
        for (uint i = 0; i < targetProposal.voters.length; i++) {
            require(
                targetProposal.voters[i] != msg.sender,
                "You have already voted!"
            );
        }
        for (uint i = 0; i < targetProposal.voteOptions.length; i++) {
            if (
                keccak256(bytes(targetProposal.voteOptions[i])) ==
                keccak256(bytes(_voteOption))
            ) {
                targetProposal.voteOptionsCount[i]++;
            }
        }
        targetProposal.voters.push(msg.sender);
    }

    function getStatsOnOption(
        uint256 _proposalId,
        string memory _voteOption
    ) public view returns (uint256 targetOptionCount) {
        require(_proposalId <= maxId, "Wrong proposal ID!");
        Proposal storage targetProposal = proposals[_proposalId];
        for (uint i = 0; i < targetProposal.voteOptions.length; i++) {
            if (
                keccak256(bytes(targetProposal.voteOptions[i])) ==
                keccak256(bytes(_voteOption))
            ) {
                return targetProposal.voteOptionsCount[i];
            }
        }
    }

    function getStatsOnProposal(
        uint256 _proposalId
    ) public view returns (uint256[] memory, string[] memory) {
        require(_proposalId <= maxId, "Wrong proposal ID!");
        Proposal storage targetProposal = proposals[_proposalId];
        return (targetProposal.voteOptionsCount, targetProposal.voteOptions);
    }

    function finishProposal(uint256 _proposalId) public {
        require(_proposalId <= maxId, "Wrong proposal ID!");
        Proposal storage targetProposal = proposals[_proposalId];
        require(
            msg.sender == targetProposal.owner,
            "You are not the owner of proposal!"
        );
        uint256 maxCount = 0;
        for (uint i = 0; i < targetProposal.voteOptions.length; i++) {
            if (targetProposal.voteOptionsCount[i] > maxCount) {
                targetProposal.winner = targetProposal.voteOptions[i];
            }
        }
        targetProposal.closed = true;
    }

    function getAllRunningSession() public view returns (uint256[] memory) {
        uint256[] memory runningIds = new uint256[](maxId + 1);
        uint256 count = 0;
        for (uint256 i = 0; i <= maxId; i++) {
            if (!proposals[i].closed) {
                runningIds[count] = i;
                count++;
            }
        }
        uint256[] memory result = new uint256[](count - 1);
        for (uint256 i = 0; i < count-1; i++) {
            result[i] = runningIds[i];
        }
        return result;
    }

    function getWinner(uint256 _proposalId) public view returns (string memory) {
        require(proposals[_proposalId].closed, "Voting is still running");
        require(_proposalId <= maxId, "Wrong proposal ID!");
        return proposals[_proposalId].winner;
    }
}
