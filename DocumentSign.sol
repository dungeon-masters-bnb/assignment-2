// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract DocumentSign {

    struct Signer {
        bool hasSign;
        bool canSign;
    }

    struct Document {
        string name;
        bool isSigned;
        uint256 signLeft;
    }

    Document public document;
    mapping (address => Signer) signers;


    constructor(address[] memory _signers, string memory _docName) {
        document.name = _docName;
        for(uint256 i = 0; i < _signers.length; i++) {
            addSigner(_signers[i]);
        }
    }
    

    function addSigner(address _signer) private {
        require(!document.isSigned, "Document is signed");
        signers[_signer].canSign = true;
        document.signLeft += 1;
    }


    function sign() public {
        require(signers[msg.sender].canSign, "You can't sign.");
        signers[msg.sender].hasSign = true;
        signers[msg.sender].canSign = false;

        document.signLeft -= 1;
        if(document.signLeft == 0) {
            document.isSigned = true;
        }
    }

    function checkDocumentSign() public view returns(bool) {
        return document.isSigned;
    }
}